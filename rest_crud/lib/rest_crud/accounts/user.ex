defmodule RestCrud.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias RestCrud.Accounts.User


  schema "users" do
    field :email, :string
    field :name, :string
    field :userId, :string

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:userId, :name, :email])
    |> validate_required([:userId, :name, :email])
  end
end
