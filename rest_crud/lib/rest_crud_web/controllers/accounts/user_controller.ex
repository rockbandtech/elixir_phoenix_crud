defmodule RestCrudWeb.Accounts.UserController do
  use RestCrudWeb, :controller

  import Ecto.Query, only: [from: 2]
  require Logger
  
  alias RestCrud.Accounts
  alias RestCrud.Accounts.User
  alias RestCrud.Repo
  
  action_fallback RestCrudWeb.FallbackController

  def create_user(conn,params) do
    case Accounts.create_user(params) do
      {:ok, user} -> json conn, %{
        "result": "success",
        "data": params
      }
    end
    # changeset = User.changeset(%User{},params)
    # if changeset.valid? do
    #   Repo.insert(changeset)
      
    #   json conn, %{
    #     "result": "success",
    #     "data": params
    #   }
    # else
    #   json conn, %{
    #     "result": "failed",
    #     "message": "sorry"
    #   }
    # end
  end
  
  def query_users(conn, params) do
    #users = Accounts.list_users()
    # list_users = Repo.all(User)
    users = Repo.all(User)
    render(conn, "index.json", users: users)
  end
  
  def update_user(conn, params) do
    
    query_cmd = 
      from(
        u in User,
        where: u.userId == ^params["userId"],
        select: u
      )
    user = User
    user = Repo.one(query_cmd)
    Accounts.update_user(user,params)
    # changeset = User.changeset(user,params)
    # Repo.update(changeset)
    # Repo.all(query_cmd) |> Enum.map(
    #   fn(u)->
    #     Repo.update(User.changeset(Repo.get!(User, 1), params))
    #   end
    # )
    json conn, %{
      "result": "Test",
      }
  end
  
  def delete_user(conn, params) do
    query_cmd = 
      from(
        u in User,
        where: u.userId == ^params["userId"],
        select: u
      )
    user = User
    user = Repo.one(query_cmd)
    Accounts.delete_user(user)
    # changeset = User.changeset(user,params)
    # Repo.delete(changeset)
    json conn, %{
      "result": "Test",
      }
  end
  
end
