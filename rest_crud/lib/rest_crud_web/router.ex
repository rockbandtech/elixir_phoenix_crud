defmodule RestCrudWeb.Router do
  use RestCrudWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", RestCrudWeb do
    pipe_through :api
    scope     "/accounts", Accounts do
      post    "/create", UserController , :create_user
      get     "/users",  UserController, :query_users
      put     "/update", UserController, :update_user
      delete  "/delete", UserController, :delete_user
      #resources "/users", UserController, except: [:new, :edit]
    end
  end
end
