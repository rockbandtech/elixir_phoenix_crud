defmodule RestCrudWeb.Accounts.UserView do
  use RestCrudWeb, :view
  alias RestCrudWeb.Accounts.UserView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      userId: user.userId,
      name: user.name,
      email: user.email}
  end
end
