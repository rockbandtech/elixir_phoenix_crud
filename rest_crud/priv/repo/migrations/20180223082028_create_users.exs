defmodule RestCrud.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :userId, :text
      add :name, :string
      add :email, :string

      timestamps()
    end

  end
end
